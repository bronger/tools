Bronger Go tools
================

This is a collection of packages that I plan to use in my own projects on a
regular basis.  They reflect my best practices for the respective topics.


Error handling
--------------

Fundamental considerations
..........................

- Panic only on unexpected errors.
- Errors are resolved locally wherever possible – however, in most cases, it is
  not possible.
- Functions that can return values return unresolved errors.
- Functions that can not return values (main, init, goroutines without an error
  backchannel) *and* are not in library code, call ``ExitOnExpectedError`` or
  ``ExitWithExpectedError``.


Importing the package
.....................

My package is imported this way:

.. code-block:: go

  import (
  	tbr_errors "gitlab.com/bronger/tools/errors"
  )


Unexpected errors
.................

- Mean that there is a bug in the code.
- Must abort the program as soon as possible.
- Need a stack trace.

Unexpected errors are internal errors.  The code itself is not trustworthy any
more.  They fall into the “this can never happen” category.  In particular, any
problem caused by external players (disk full, invalid response from server)
never is unexpected.  It may mean that my own (library) code is buggy, but it
can also mean that the caller called my code wrongly.

An interesting borderline case is if an external service returns something that
doesn’t make sense.  Who’s to blame?  If it is highly unlikely that the
external service is broken, one may consider it an unexpected error.

In short: Unexpected errors are made in the coding phase, and expected errors
in the running phase.

If an unexpected error occurs, the code should panic immediately, even library
code.


Discussion
,,,,,,,,,,

Previously, I returned error values but this ended up being a “poor man’s
panic”.  The disadvantages were:

- It may be difficult for the caller to distinguish between expected and
  unexpected errors post-mortem.
- It was difficult to get a decent stack trace, especially in the error output.
  slog’s default formatting is awful for this.
- I relied on the archived, unmaintained pkg/errors package.
- Some functions had an error return value just for such unexpected errors.
  And every error return value is a burden.
- I exited immediately anyway.

The only downside is that a small library may abort a big program, and the
program cannot do much about it.  Well, I can live with this.  Again, a buggy
program means that daemons may fly out of your nose, and this should not
proceed any further.

Besides, Go panics implicitly for all sorts of things (on the language and the
standard lib level), and we live with that well.


Expected errors
...............

- Don’t mean that there is a bug in the code.
- Mean that the environment of the program (network, disks, user) does not work
  properly.
- Usually don’t abort the program.
- Don’t need a stack trace.
- May be sentinels (name convention: ``ErrSomething``).
- May be bespoke error types (name convention: ``SomethingError``).
- To wrap such errors is not always a good idea.  Often it is better to return
  a new error value instead, so that no implementation details leak through the
  API.

So, such errors are either resolved locally or passed to the caller, wrapped or
– preferably – as a new error value.


How to create expected errors
,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

You do so using

- ``errors.New``
- ``fmt.Errorf``
- Instantiation of a custom error type

In general, sentinels are a good idea for such errors.


How to pass expected errors
,,,,,,,,,,,,,,,,,,,,,,,,,,,

You do so either by replacing it with a new sentinel, or by using
``fmt.Errorf`` in conjunction with ``%w``.


Fatal expected errors
,,,,,,,,,,,,,,,,,,,,,

- Mean a misuse of the program, or a failure in its environment (e.g. full file
  system).
- Must abort the program as soon as possible because safe and reliable
  proceeding is not possible.
- This aborting occurs only in the ``main`` package.  Library code just passes
  such errors.

The error is logged, and then the program ends with ``os.Exit``.  The exit code
must be greater than 1.  (1 is reserved for unexpected errors.)  I reserve Exit
code 10 for “miscellaneous expected errors” to avoid any small program being
forced to maintain and document a list of exit codes.  In practice, you
terminate the program using

- ``tbr_errors.ExitOnExpectedError``
- ``tbr_errors.ExitWithExpectedError``

For the proper exit code, your ``main()`` function must begin with

.. code-block:: go

     defer func() { tbr_errors.Handle(recover()) }()

This deferred function – or others defined afterwards – may do additional
clean-up before the program termination.


Logging
-------

Fundamental considerations
..........................

- Library code logs only below the error level (warn, info, debug).  In
  contrast, errors are resolved or returned to the caller.
- Library code logs as little as possible.  Positive examples are: Documenting
  when errors are resolved locally, and everything on the debug level.
- Library code does not instantiate its own logging facility.  If at all, it
  takes an injected logger for that.


Initialising logging
....................

Somewhere in the main package, one writes:

.. code-block:: go

  import (
  	tbr_logging "gitlab.com/bronger/tools/logging"
  )

  func init() {
  	tbr_logging.Init(os.Stderr, slog.LevelDebug)
  }


Injecting loggers into libraries
................................

The library functions should accept an argument of the ``Logger`` interface –
preferably as the first argument, or second to ``ctx``:

.. code-block:: go

  func MyFunction(ctx context.Context, logger tbr_logging.Logger) error {
  	logger.Info("In function!")
  }

In the main package, you may call this function with:

.. code-block:: go

  thatLibLogger := slog.With("component", "thatLib")
  …
  err = thatLib.MyFunction(ctx, thatLibLogger)


Licence
-------

Copyright © 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
