/*
	bronger/tools – Go tools of Torsten Bronger

Copyright (C) 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package tbr_errors

import (
	"log/slog"
	"os"
)

// ExitOnExpectedError aborts the program in case of an expected error, if a
// non-nil error value is passed.  See the README for a detailed explanation of
// “expected error”.  It is supposed to be called in the main package, where we
// know that we must abort.  Use [Handle] to properly handle the panic in the
// main function.
func ExitOnExpectedError(e error, msg string, exitCode int, args ...any) {
	if e != nil {
		args = append([]any{"error", e}, args...)
		ExitWithExpectedError(msg, exitCode, args...)
	}
}

type fatalError struct {
	exitCode int
}

// ExitWithExpectedError aborts the program in case of an expected error.  See
// the README for a detailed explanation.  It is supposed to be called in the
// main package, where we know that we must abort.  Use [Handle] to properly
// handle the panic in the main function.
func ExitWithExpectedError(msg string, exitCode int, args ...any) {
	if exitCode < 2 {
		panic("Invalid exit code")
	}
	slog.Error(msg, args...)
	panic(fatalError{exitCode})
}

// Handle converts a panic from ExitOnExpectedError and ExitWithExpectedError
// into a termination of the program with the proper exit code.  Place `defer
// func() { tbr_errors.Handle(recover()) }()` at the very top of your main
// function.
func Handle(e_ any) {
	if e_ != nil {
		if e, ok := e_.(fatalError); ok {
			os.Exit(e.exitCode)
		} else {
			panic(e_)
		}
	}
}
