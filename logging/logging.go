/*
	bronger/tools – Go tools of Torsten Bronger

Copyright (C) 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package tbr_logging

import (
	"io"
	"log/slog"
)

// Logger may be used as the type for library function arguments.  This way,
// you can inject a logger into the library as an explicit dependency.  It is
// designed so that slog.Default() could be used straight away as a value.  For
// loggers with differing function signatures, you probably have to write a
// wrapper in the main package.  Note that errors should not be logged by
// libraries but reported to the caller.
type Logger interface {
	Warn(msg string, args ...any)
	Info(msg string, args ...any)
	Debug(msg string, args ...any)
	With(args ...any) *slog.Logger
}

// Init sets up logging with slog.
func Init(w io.Writer, level slog.Level) {
	th := slog.NewTextHandler(w, &slog.HandlerOptions{
		Level: level,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				a.Value = slog.StringValue(a.Value.Time().Format("2006-01-02 15:04:05"))
			}
			return a
		},
	})
	logger := slog.New(th)
	slog.SetDefault(logger)
}
